package org.raghu;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        GraphTest.class,
        WeightedGraphTest.class
})
public class GraphTestSuite {

}
