package org.raghu;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;


public class GraphTest {

    private static Graph directedGraphFixture, undirectedGraphFixture, connectedGraphFixture, notConnectedGraphFixture;

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass");
        directedGraphFixture = new Graph(true);
        Vertex dv1 = directedGraphFixture.addVertex("dv1");
        Vertex dv2 = directedGraphFixture.addVertex("dv2");
        Vertex dv3 = directedGraphFixture.addVertex("dv3");

        directedGraphFixture.addEdge(dv1, dv2);
        directedGraphFixture.addEdge(dv1, dv3);

        undirectedGraphFixture = new Graph(false);
        Vertex udv1 = undirectedGraphFixture.addVertex("udv1");
        Vertex udv2 = undirectedGraphFixture.addVertex("udv2");
        Vertex udv3 = undirectedGraphFixture.addVertex("udv3");

        undirectedGraphFixture.addEdge(udv1, udv2);
        undirectedGraphFixture.addEdge(udv1, udv3);

        connectedGraphFixture = createConnectedGraph();
        notConnectedGraphFixture = createNotConnectedGraph();

    }

    private static Graph createConnectedGraph(){
        Graph simpleGraph1 = new Graph(false);

        Vertex a = simpleGraph1.addVertex("A");
        Vertex b = simpleGraph1.addVertex("B");
        Vertex c = simpleGraph1.addVertex("C");
        Vertex d = simpleGraph1.addVertex("D");
        Vertex e = simpleGraph1.addVertex("E");
        Vertex f = simpleGraph1.addVertex("F");
        Vertex g = simpleGraph1.addVertex("G");
        Vertex h = simpleGraph1.addVertex("H");
        Vertex i = simpleGraph1.addVertex("I");

        simpleGraph1.addEdge( a, i);
        simpleGraph1.addEdge( i, d);
        simpleGraph1.addEdge(a, b);
        simpleGraph1.addEdge(a, c);
        simpleGraph1.addEdge(b, c);
        simpleGraph1.addEdge(c, d);

        simpleGraph1.addEdge(e, h);
        simpleGraph1.addEdge(e, f);
        simpleGraph1.addEdge(e, g);
        simpleGraph1.addEdge(f, h);
        simpleGraph1.addEdge(f, i);
        simpleGraph1.addEdge(i, g);
        simpleGraph1.addEdge(g, h);

        return simpleGraph1;

    }



    private static Graph createNotConnectedGraph(){
        Graph simpleGraph = new Graph(false);
        Vertex a = simpleGraph.addVertex("A");
        Vertex b = simpleGraph.addVertex("B");
        Vertex c = simpleGraph.addVertex("C");
        Vertex d = simpleGraph.addVertex("D");
        Vertex e = simpleGraph.addVertex("E");
        Vertex f = simpleGraph.addVertex("F");
        Vertex g = simpleGraph.addVertex("G");
        Vertex h = simpleGraph.addVertex("H");

        simpleGraph.addEdge( a, d);
        simpleGraph.addEdge(a, b);
        simpleGraph.addEdge(a, c);
        simpleGraph.addEdge(b, c);
        simpleGraph.addEdge(c, d);

        simpleGraph.addEdge(e, h);
        simpleGraph.addEdge(e, f);
        simpleGraph.addEdge(e, g);
        simpleGraph.addEdge(f, h);
        simpleGraph.addEdge(f, g);
        simpleGraph.addEdge(g, h);

        return simpleGraph;
    }

    @Test
    public void testAddVertex() {
        Graph graph = new Graph(false);
        Vertex v1 = graph.addVertex("v1");
        Vertex v2 = graph.addVertex("v2");
        assertEquals(2, graph.vertexesList.size());
        assertEquals(v1, graph.vertexesList.get(0));
        assertEquals(v2, graph.vertexesList.get(1));
        assertEquals("v1", graph.vertexesList.get(0).getName());
        assertEquals("v2", graph.vertexesList.get(1).getName());
    }

    @Test
    public void testGetVertexByName() {
        assertEquals("dv1", directedGraphFixture.getVertexByName("dv1").getName());
        assertEquals("dv2", directedGraphFixture.getVertexByName("dv2").getName());

        assertEquals("udv1", undirectedGraphFixture.getVertexByName("udv1").getName());
        assertEquals("udv2", undirectedGraphFixture.getVertexByName("udv2").getName());
    }

    @Test
    public void testAddEdgeForUndirectedGraph () {
        Graph graph = new Graph(false);
        Vertex v1 = graph.addVertex("v1");
        Vertex v2 = graph.addVertex("v2");
        Edge e1 = graph.addEdge(v1, v2);
        Iterable<Edge> e = graph.getEdgesForVertex(v1);
        assertTrue(e.iterator().hasNext());
        Edge e2 = e.iterator().next();
        assertEquals(e1, e2);

        e = graph.getEdgesForVertex(v2);
        assertTrue(e.iterator().hasNext());
        e2 = e.iterator().next();
        assertEquals(v2, e2.getEndPoints()[0]);
        assertEquals(v1, e2.getEndPoints()[1]);
    }

    @Test
    public void testAddEdgeForDirectedGraph () {
        Graph graph = new Graph(true);
        Vertex v1 = graph.addVertex("v1");
        Vertex v2 = graph.addVertex("v2");
        Edge e1 = graph.addEdge(v1, v2);
        Iterable<Edge> e = graph.getEdgesForVertex(v1);
        assertTrue(e.iterator().hasNext());
        Edge e2 = e.iterator().next();
        assertEquals(e1, e2);

        e = graph.getEdgesForVertex(v2);
        assertTrue(!e.iterator().hasNext());
    }

    @Test
    public void testRemoveEdge() {
        Graph graph = new Graph(false);
        Vertex v1 = graph.addVertex("v1");
        Vertex v2 = graph.addVertex("v2");
        Edge e1 = graph.addEdge(v1, v2);

        Iterable<Edge> e = graph.getEdgesForVertex(v1);
        assertTrue(e.iterator().hasNext());
        assertEquals(e1, e.iterator().next());
        e = graph.getEdgesForVertex(v2);
        assertTrue(e.iterator().hasNext());

        graph.removeEdge(e1);
        e = graph.getEdgesForVertex(v1);
        assertTrue(!e.iterator().hasNext());

        e = graph.getEdgesForVertex(v2);
        assertTrue(!e.iterator().hasNext());

    }

    @Test
    public void testRemoveVertex() {

        Graph simpleGraph = new Graph(false);

        Vertex a = simpleGraph.addVertex("A");
        Vertex b = simpleGraph.addVertex("B");
        Vertex c = simpleGraph.addVertex("C");
        Vertex d = simpleGraph.addVertex("D");
        Vertex vertexE = simpleGraph.addVertex("E");

        Edge ab = simpleGraph.addEdge( a, b);
        Edge ac = simpleGraph.addEdge(a, c);

        Edge bd = simpleGraph.addEdge(b, d);
        Edge cd = simpleGraph.addEdge(c, d);
        Edge de = simpleGraph.addEdge(d, vertexE);

        assertEquals (5, simpleGraph.vertexesList.size());
        simpleGraph.removeVertex(b);
        assertEquals (4, simpleGraph.vertexesList.size());

        Iterable<Edge> iterable = simpleGraph.getEdgesForVertex(a);

        iterable.forEach(e -> {
            assertNotEquals("B", e.getEndPoints()[0].getName());
            assertNotEquals("B", e.getEndPoints()[1].getName());
        });

        iterable = simpleGraph.getEdgesForVertex(d);

        iterable.forEach(e -> {
            assertNotEquals("B", e.getEndPoints()[0].getName());
            assertNotEquals("B", e.getEndPoints()[1].getName());
        });

    }

    @Test
    public void testIsConnected() {
        assertTrue(connectedGraphFixture.isConnected());
        assertTrue(!notConnectedGraphFixture.isConnected());
    }

    @Test
    public void testSerialization() throws Exception {
        FileOutputStream fout = new FileOutputStream("graph.ser", true);
        ObjectOutputStream oos = new ObjectOutputStream(fout);
        oos.writeObject(directedGraphFixture);

        InputStream streamIn = new FileInputStream("graph.ser");
        ObjectInputStream objectinputstream = new ObjectInputStream(streamIn);
        directedGraphFixture = (Graph) objectinputstream.readObject();
        assertEquals(3, directedGraphFixture.vertexesList.size());
    }

}
