package org.raghu;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class WeightedGraphTest {

    @Test
    public void testShortestPath() {
        WeightedGraph weightedGraph = new WeightedGraph(true);

        Vertex a1 = weightedGraph.addVertex("1");
        Vertex a2 = weightedGraph.addVertex("2");
        Vertex a3 = weightedGraph.addVertex("3");
        Vertex a4 = weightedGraph.addVertex("4");
        Vertex a5 = weightedGraph.addVertex("5");
        Vertex a6 = weightedGraph.addVertex("6");
        Vertex a7 = weightedGraph.addVertex("7");
        Vertex a8 = weightedGraph.addVertex("8");

        weightedGraph.addEdge(a1, a2, 3);
        weightedGraph.addEdge(a1, a3, 1);
        weightedGraph.addEdge(a2, a4, 3);
        weightedGraph.addEdge(a2, a5, 5);
        weightedGraph.addEdge(a4, a5, 1);
        weightedGraph.addEdge(a4, a7, 10);
        weightedGraph.addEdge(a5, a7, 2);
        weightedGraph.addEdge(a5, a8, 8);
        weightedGraph.addEdge(a5, a6, 2);
        weightedGraph.addEdge(a5, a3, 8);
        weightedGraph.addEdge(a7, a8, 5);
        weightedGraph.addEdge(a3, a6, 5);
        weightedGraph.addEdge(a6, a8, 15);

        Route aRoute = weightedGraph.shortestPath(a1, a8);
        assertEquals(14, aRoute.getCost());
        assertEquals(a1, aRoute.getPathList().get(0));
        assertEquals(a2, aRoute.getPathList().get(1));
        assertEquals(a4, aRoute.getPathList().get(2));
        assertEquals(a5, aRoute.getPathList().get(3));
        assertEquals(a7, aRoute.getPathList().get(4));
        assertEquals(a8, aRoute.getPathList().get(5));

    }





}
