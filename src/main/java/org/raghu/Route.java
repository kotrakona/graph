package org.raghu;

import java.util.List;



/**
 * The Class Route.
 */
public class Route {
    
    /** The cost. */
    private int cost;

    /** The path list. */
    private List<Vertex> pathList;

    /**
     * Instantiates a new route.
     *
     * @param cost the cost
     * @param pathList the path list
     */
    public Route(int cost, List<Vertex> pathList) {
        this.cost = cost;
        this.pathList = pathList;
    }

    /**
     * Gets the cost.
     *
     * @return the cost
     */
    public int getCost() {
        return cost;
    }

    /**
     * Gets the path list.
     *
     * @return the path list
     */
    public List<Vertex> getPathList() {
        return pathList;
    }
}
