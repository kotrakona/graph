package org.raghu;

import java.io.Serializable;
import java.util.*;



/**
 * The Class WeightedGraph.
 */
public class WeightedGraph extends Graph implements Serializable {

    /**
     * Instantiates a new weighted graph.
     *
     * @param directed the directed
     */
    public WeightedGraph(boolean directed) {
        super(directed);
    }

    /**
     * Adds the edge.
     *
     * @param v1 the v1
     * @param v2 the v2
     * @param weight the weight
     * @return the edge
     */
    public Edge addEdge(Vertex v1, Vertex v2, Integer weight) {
        Edge fromEdge = new Edge(v1, v2, weight);
        v1.getEdgeMap().put(v2, fromEdge);
        if (!directed) {
            Edge toEdge = new Edge(v2, v1, weight);
            v2.getEdgeMap().put(v1, toEdge);
        }
        return fromEdge;
    }

    /**
     * Shortest path.
     *
     * @param startVertex the start vertex
     * @param endVertex the end vertex
     * @return the route
     */
    public Route shortestPath(Vertex startVertex, Vertex endVertex) {
        List<WeightedVertex> weightedVertexList = new ArrayList<>();
        Map<Vertex, Integer> distanceMap = new HashMap<>();
        Map<Vertex, Vertex> parentMap = new HashMap<>();

        final Comparator<WeightedVertex> comp = (p1, p2) -> Integer.compare(p1.getWeight(), p2.getWeight());

        this.vertexesList.stream().forEach(e -> {
            WeightedVertex wVertex = new WeightedVertex(e, Integer.MAX_VALUE);
            weightedVertexList.add(wVertex);
        });
        weightedVertexList.stream().filter(e -> e.getVertex() == startVertex).findFirst().get().setWeight(0);
        distanceMap.put(startVertex, 0);
        parentMap.put(startVertex, null);

        while (!weightedVertexList.isEmpty()) {
            WeightedVertex wx = weightedVertexList.stream().min(comp).get();

            weightedVertexList.remove(wx);
            Vertex current = wx.getVertex();
            distanceMap.put(current, wx.getWeight());
            for (Edge edge : current.getEdgeMap().values()) {
                Vertex adjacent = edge.getEndPoints()[1];
                if (!weightedVertexList.stream().filter(e-> e.getVertex()==adjacent).findFirst().isPresent()) {
                    continue;
                }
                int newDistance = distanceMap.get(current) + edge.getWeight();
                WeightedVertex w = weightedVertexList.stream().filter(e-> e.getVertex()==adjacent).findFirst().get();
                if (w.getWeight() > newDistance) {
                    w.setWeight(newDistance);
                    parentMap.put(adjacent, current);
                }
            }
        }
        List <Vertex> pathList = getPathFromMap(parentMap, startVertex, endVertex);
        Route aRoute = new Route(distanceMap.get(endVertex), pathList);
        return aRoute;
    }

    /**
     * Gets the path from map.
     *
     * @param parentMap the parent map
     * @param startVertex the start vertex
     * @param endVertex the end vertex
     * @return the path from map
     */
    private List <Vertex> getPathFromMap (Map <Vertex, Vertex> parentMap, Vertex startVertex, Vertex endVertex){
        List <Vertex> pathList = new ArrayList<>();
        pathList.add(endVertex);
        Vertex aVertex =  parentMap.get(endVertex);
        while (aVertex != startVertex) {
            pathList.add(aVertex);
            aVertex =  parentMap.get(aVertex);
        }
        pathList.add(aVertex);
        Collections.reverse(pathList);
        return pathList;
    }

    /**
     * The Class WeightedVertex.
     */
    private class WeightedVertex  {
        
        /** The vertex. */
        Vertex vertex;
        
        /** The weight. */
        Integer weight;

        /**
         * Instantiates a new weighted vertex.
         *
         * @param vertex the vertex
         * @param weight the weight
         */
        public WeightedVertex(Vertex vertex, Integer weight) {
            this.vertex = vertex;
            this.weight = weight;
        }

        /**
         * Gets the vertex.
         *
         * @return the vertex
         */
        public Vertex getVertex() {
            return vertex;
        }

        /**
         * Sets the vertex.
         *
         * @param vertex the new vertex
         */
        public void setVertex(Vertex vertex) {
            this.vertex = vertex;
        }

        /**
         * Gets the weight.
         *
         * @return the weight
         */
        public Integer getWeight() {
            return weight;
        }

        /**
         * Sets the weight.
         *
         * @param weight the new weight
         */
        public void setWeight(Integer weight) {
            this.weight = weight;
        }
    }
    
}
