package org.raghu;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * The Class Vertex.
 */
public class Vertex implements Serializable {
    
    /** The name. */
    private String name;

    /** The edge map. */
    private Map<Vertex, Edge> edgeMap = new HashMap<>();

    /**
     * Instantiates a new vertex.
     *
     * @param name the name
     */
    public Vertex(String name) {
        this.name = name;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the edge map.
     *
     * @return the edge map
     */
    public Map<Vertex, Edge> getEdgeMap() {
        return edgeMap;
    }

}
