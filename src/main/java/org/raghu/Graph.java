package org.raghu;

import java.io.Serializable;
import java.util.*;


/**
 * The Class Graph.
 */
public class Graph implements Serializable {

    /** The directed. */
    protected boolean directed = false;
    
    /** The vertexes list. */
    protected List<Vertex> vertexesList = new ArrayList<>();


    /**
     * Instantiates a new graph.
     *
     * @param directed the directed
     */
    public Graph(boolean directed) {
        this.directed = directed;
    }

    /**
     * Adds the vertex.
     *
     * @param name the name
     * @return the vertex
     */
    public Vertex addVertex(String name) {
        Vertex aVertex = new Vertex(name);
        this.vertexesList.add(aVertex);
        return aVertex;
    }

    /**
     * Removes the vertex.
     *
     * @param v the v
     */
    public void removeVertex(Vertex v) {
        while (!v.getEdgeMap().values().isEmpty()) {
            Edge edge = v.getEdgeMap().values().stream().findFirst().get();
            removeEdge(edge);
        }
        this.vertexesList.remove(v);
    }

    /**
     * Adds the edge.
     *
     * @param v1 the v1
     * @param v2 the v2
     * @return the edge
     */
    public Edge addEdge( Vertex v1, Vertex v2) {
        Edge fromEdge = new Edge(v1, v2);
        v1.getEdgeMap().put(v2, fromEdge);
        if (!directed) {
            Edge toEdge = new Edge(v2, v1);
            v2.getEdgeMap().put(v1, toEdge);
        }
        return fromEdge;
    }

    /**
     * Removes the edge.
     *
     * @param edge the edge
     */
    public void removeEdge(Edge edge) {
        Vertex[] vertexes = edge.getEndPoints();
        vertexes[0].getEdgeMap().remove(edge.getEndPoints()[1]);
        if (!directed) {
            vertexes[1].getEdgeMap().remove(edge.getEndPoints()[0]);
        }
    }

    /**
     * Gets the vertex by name.
     *
     * @param vertexName the vertex name
     * @return the vertex by name
     */
    public Vertex getVertexByName(String vertexName) {
        return vertexesList.stream().filter(e -> e.getName().equalsIgnoreCase(vertexName)).findFirst().get();
    }

    /**
     * Gets the edges for vertex.
     *
     * @param v the v
     * @return the edges for vertex
     */
    public Iterable<Edge> getEdgesForVertex(Vertex v) {
        return v.getEdgeMap().values();
    }

    /**
     * search the graph for connectedness.
     *
     * @param v the v
     * @param processedVertexesList the vertexes that have already been processed
     * @param forest the forest
     */
    private void search(Vertex v, Set<Vertex> processedVertexesList, Map<Vertex, Edge> forest) {
        processedVertexesList.add(v);
        for (Edge e : this.getEdgesForVertex(v)) {
            Vertex v1 = e.getEndPoints()[1];
            if (!processedVertexesList.contains(v1)) {
                forest.put(v1, e);
                search(v1, processedVertexesList, forest);
            }
        }
    }

    /**
     * Returns true if the graph isConnected.
     *
     * @return the map
     */
    public boolean isConnected() {
        Set<Vertex> processedVertxesList = new HashSet<>();
        Map<Vertex, Edge> forest = new HashMap<>();
        search(vertexesList.get(0), processedVertxesList, forest);
        return (forest.size() == vertexesList.size() - 1);

    }


}
