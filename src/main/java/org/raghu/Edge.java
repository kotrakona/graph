package org.raghu;

import java.io.Serializable;

/**
 * The Class Edge.
 */
public class Edge implements Serializable{

    
    /** The weight. */
    private int weight;
    
    
    /** The end points. */
    private Vertex[] endPoints;

    
    /**
     * Instantiates a new edge.
     *
     * @param v1 the v1
     * @param v2 the v2
     */
    public Edge(Vertex v1, Vertex v2) {
        this.endPoints = new Vertex[]{v1,v2};
    }

    
    /**
     * Instantiates a new edge.
     *
     * @param v1 the v1
     * @param v2 the v2
     * @param weight the weight
     */
    public Edge(Vertex v1, Vertex v2, int weight) {
        this (v1, v2);
        this.weight = weight;
    }



    
    /**
     * Gets the weight.
     *
     * @return the weight
     */
    public int getWeight() {
        return weight;
    }

    
    /**
     * Sets the weight.
     *
     * @param weight the new weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    
    /**
     * Gets the end points.
     *
     * @return the end points
     */
    public Vertex[] getEndPoints() {
        return endPoints;
    }


    /**
     * Sets the end points.
     *
     * @param endPoints the new end points
     */
    public void setEndPoints(Vertex[] endPoints) {
        this.endPoints = endPoints;
    }
}
